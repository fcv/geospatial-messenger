import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	val kotlinVersion = "1.9.24"
	kotlin("jvm") version kotlinVersion
	kotlin("plugin.spring") version kotlinVersion

	id("org.springframework.boot") version "3.2.5"
	id("io.spring.dependency-management") version "1.1.5"
	id("org.asciidoctor.jvm.convert") version "4.0.2"
}

tasks {
	withType<KotlinCompile> {
		kotlinOptions {
			jvmTarget = "17"
			freeCompilerArgs = listOf("-Xjsr305=strict")
		}
	}
}

repositories {
	mavenCentral()
	// so we can pull com.github.fcv:postgis-geojson dependency
	// see https://github.com/fcv/postgis-geojson
	// see https://jitpack.io/#fcv/postgis-geojson/3.1
	maven("https://jitpack.io")
}

dependencies {
	val exposedVersion = "0.50.1"
	val testcontainersVersion = "1.19.8"

	implementation("org.springframework.boot:spring-boot-starter-web") {
		exclude(module = "spring-boot-starter-validation")
	}
	implementation("org.springframework.boot:spring-boot-starter-jdbc")
	implementation("org.springframework.boot:spring-boot-devtools")

	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

	implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
	implementation("org.jetbrains.exposed:spring-transaction:$exposedVersion")
	implementation("org.postgresql:postgresql")
	implementation("net.postgis:postgis-jdbc:2023.1.0") {
		exclude(module = "postgresql")
	}
	implementation("com.github.fcv:postgis-geojson:3.1")  {
		exclude(module = "postgresql")
	}

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.restdocs:spring-restdocs-mockmvc")
	testImplementation("org.testcontainers:testcontainers:$testcontainersVersion")
	testImplementation("org.testcontainers:junit-jupiter:$testcontainersVersion")
	testImplementation("org.testcontainers:postgresql:$testcontainersVersion")
}

val snippetsDir by extra { file("build/generated-snippets") }

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.test {
	outputs.dir(snippetsDir)
}

tasks.asciidoctor {
	inputs.dir(snippetsDir)
	dependsOn(tasks.test)
}
