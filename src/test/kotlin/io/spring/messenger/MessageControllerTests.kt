package io.spring.messenger

import com.fasterxml.jackson.databind.ObjectMapper
import io.spring.messenger.domain.Message
import io.spring.messenger.domain.User
import io.spring.messenger.repository.MessageRepository
import io.spring.messenger.repository.UserRepository
import net.postgis.jdbc.geometry.Point
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.restdocs.RestDocumentationContextProvider
import org.springframework.restdocs.RestDocumentationExtension
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler
import org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest
import org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse
import org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint
import org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import org.springframework.restdocs.request.RequestDocumentation.pathParameters
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import org.springframework.web.context.WebApplicationContext
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@SpringBootTest
@Testcontainers
@ExtendWith(RestDocumentationExtension::class, SpringExtension::class)
class MessageControllerTests {

    @Autowired lateinit   var messageRepository:  MessageRepository
    @Autowired lateinit   var userRepository:     UserRepository
    @Autowired lateinit   var mapper:             ObjectMapper
               lateinit   var mockMvc:            MockMvc
               lateinit   var document:           RestDocumentationResultHandler

    @BeforeEach fun setUp(context: WebApplicationContext,
                          restDocumentation: RestDocumentationContextProvider) {
        messageRepository.deleteAll()
        userRepository.deleteAll()
        document = document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()));
        mockMvc = webAppContextSetup(context)
            .apply<DefaultMockMvcBuilder>(MockMvcRestDocumentation.documentationConfiguration(restDocumentation))
            .build()
    }

    @Test fun listMessages() {
        userRepository.create(User("swhite", "Skyler", "White"))
        messageRepository.create(Message("foo", "swhite"))
        messageRepository.create(Message("bar", "swhite", Point(0.0, 0.0)))
        mockMvc.perform(get("/message")
            .accept(APPLICATION_JSON))
            .andExpect(status().isOk)
            .andDo(document.document(responseFields(
              fieldWithPath("[].id").description("The message ID"),
              fieldWithPath("[].content").description("The message content"),
              fieldWithPath("[].author").description("The message author username"),
              fieldWithPath("[].location").optional().description("Optional, the message location (latitude, longitude)"),
                fieldWithPath("[].location.type").optional().description("The coordinate type"),
                fieldWithPath("[].location.coordinates").optional().description("Optional, the user location's coordinate (latitude, longitude)"),
            )))
    }

    @Test fun createMessage() {
        userRepository.create(User("swhite", "Skyler", "White"))
        var message = Message("""Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
        minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum."""
                , "swhite", Point(0.0, 0.0))
        mockMvc.perform(post("/message")
                .content(mapper.writeValueAsString(message))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isCreated)
                .andDo(document.document(
                    requestFields(
                        fieldWithPath("content").description("The message content"),
                        fieldWithPath("author").description("The message author username"),
                        fieldWithPath("location").optional().description("Optional, the message location (latitude, longitude)"),
                        fieldWithPath("location.type").optional().description("The coordinate type"),
                        fieldWithPath("location.coordinates").optional().description("Optional, the user location's coordinate (latitude, longitude)"),
                    ),
                    responseFields(
                        fieldWithPath("id").description("The message ID"),
                        fieldWithPath("content").description("The message content"),
                        fieldWithPath("author").description("The message author username"),
                        fieldWithPath("location").optional().description("Optional, the message location (latitude, longitude)"),
                        fieldWithPath("location.type").optional().description("The coordinate type"),
                        fieldWithPath("location.coordinates").optional().description("Optional, the user location's coordinate (latitude, longitude)"),
                    )
                ))
    }

    @Test fun findMessagesByBoundingBox() {
        userRepository.create(User("swhite", "Skyler", "White"))
        messageRepository.create(Message("foo", "swhite", Point(0.0, 0.0)))
        messageRepository.create(Message("bar", "swhite", Point(1.0, 1.0)))

        mockMvc.perform(get("/message/bbox/{xMin},{yMin},{xMax},{yMax}", -1, -1, 2, 2)
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk)
                .andDo(document.document(
                    pathParameters(
                        parameterWithName("xMin").description("The latitude of the lower-left corner"),
                        parameterWithName("yMin").description("The longitude of the lower-left corner"),
                        parameterWithName("xMax").description("The latitude of the upper-left corner"),
                        parameterWithName("yMax").description("The longitude of the upper-left corner")
                    ),
                    responseFields(
                        fieldWithPath("[].id").description("The message ID"),
                        fieldWithPath("[].content").description("The message content"),
                        fieldWithPath("[].author").description("The message author username"),
                        fieldWithPath("[].location").optional().description("Optional, the message location (latitude, longitude)"),
                        fieldWithPath("[].location.type").optional().description("The coordinate type"),
                        fieldWithPath("[].location.coordinates").optional().description("Optional, the user location's coordinate (latitude, longitude)"),
                    )
                ))
    }

    @Test fun subscribeMessages() {
        mockMvc.perform(get("/message/subscribe")).andExpect(status().isOk)
    }

    companion object {
        @Container
        private val db = PostGISPostgreSQLContainer()
        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) = registerDatasourceProperties(db, registry)
    }

}
