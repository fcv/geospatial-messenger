package io.spring.messenger

import com.fasterxml.jackson.databind.ObjectMapper
import io.spring.messenger.domain.User
import io.spring.messenger.repository.MessageRepository
import io.spring.messenger.repository.UserRepository
import net.postgis.jdbc.geometry.Point
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.restdocs.RestDocumentationContextProvider
import org.springframework.restdocs.RestDocumentationExtension
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler
import org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest
import org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse
import org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint
import org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import org.springframework.restdocs.request.RequestDocumentation.pathParameters
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import org.springframework.web.context.WebApplicationContext
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@SpringBootTest
@Testcontainers
@ExtendWith(RestDocumentationExtension::class, SpringExtension::class)
class UserControllerTests {

    @Autowired lateinit   var userRepository:     UserRepository
    @Autowired lateinit   var messageRepository:  MessageRepository
    @Autowired lateinit   var mapper:             ObjectMapper
               lateinit   var mockMvc:            MockMvc
               lateinit   var document:           RestDocumentationResultHandler

    @BeforeEach fun setUp(context: WebApplicationContext,
                          restDocumentation: RestDocumentationContextProvider) {
        messageRepository.deleteAll()
        userRepository.deleteAll()
        document = document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()));
        mockMvc = webAppContextSetup(context)
          .apply<DefaultMockMvcBuilder>(documentationConfiguration(restDocumentation))
          .build()
    }

     @Test fun createUser() {
        var skyler = User("swhite", "Skyler", "White", Point(0.0, 0.0))
        mockMvc.perform(post("/user")
                .content(mapper.writeValueAsString(skyler))
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isCreated)
                .andDo(document.document(
                    requestFields(
                        fieldWithPath("userName").description("The user username"),
                        fieldWithPath("firstName").description("The user first name"),
                        fieldWithPath("lastName").description("The user last name"),
                        fieldWithPath("location").optional().description("Optional, the user location (latitude, longitude)"),
                        fieldWithPath("location.type").optional().description("The coordinate type"),
                        fieldWithPath("location.coordinates").optional().description("Optional, the user location's coordinate (latitude, longitude)"),
                    )
                ))
    }

    @Test fun listUsers() {
        userRepository.create(User("swhite", "Skyler", "White"))
        userRepository.create(User("jpinkman", "Jesse", "Pinkman", Point(0.0, 0.0)))

        mockMvc.perform(get("/user").accept(APPLICATION_JSON))
          .andExpect(status().isOk)
          .andDo(document.document(
              responseFields(
                  fieldWithPath("[].userName").description("The user username"),
                  fieldWithPath("[].firstName").description("The user first name"),
                  fieldWithPath("[].lastName").description("The user last name"),
                  fieldWithPath("[].location").optional().description("Optional, the user location (latitude, longitude)"),
                  fieldWithPath("[].location.type").optional().description("The coordinate type"),
                  fieldWithPath("[].location.coordinates").optional().description("Optional, the user location's coordinate (latitude, longitude)"),
              )
          ))
    }

    @Test fun findUsersByBoundingBox() {
        userRepository.create(User("swhite", "Skyler", "White", Point(0.0, 0.0)))
        userRepository.create(User("jpinkman", "Jesse", "Pinkman", Point(1.0, 1.0)))
        mockMvc.perform(get("/user/bbox/{xMin},{yMin},{xMax},{yMax}", -1, -1, 2, 2)
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk)
                .andDo(document.document(
                    pathParameters(
                        parameterWithName("xMin").description("The latitude of the lower-left corner"),
                        parameterWithName("yMin").description("The longitude of the lower-left corner"),
                        parameterWithName("xMax").description("The latitude of the upper-left corner"),
                        parameterWithName("yMax").description("The longitude of the upper-left corner")
                    ),
                    responseFields(
                        fieldWithPath("[].userName").description("The user username"),
                        fieldWithPath("[].firstName").description("The user first name"),
                        fieldWithPath("[].lastName").description("The user last name"),
                        fieldWithPath("[].location").optional().description("Optional, the user location (latitude, longitude)"),
                        fieldWithPath("[].location.type").optional().description("Optional, the user location (latitude, longitude)"),
                        fieldWithPath("[].location.coordinates").optional().description("Optional, the user location (latitude, longitude)"),

                    )
                ))
    }

     @Test fun updateUserLocation() {
         userRepository.create(User("swhite", "Skyler", "White", Point(0.0, 0.0)))
         mockMvc.perform(put("/user/{userName}/location/{x},{y}", "swhite", 1.0, 1.0))
                .andExpect(status().isNoContent)
                .andDo(document.document(
                    pathParameters(
                        parameterWithName("userName").description("The user username"),
                        parameterWithName("x").description("The new location latitude"),
                        parameterWithName("y").description("The new location latitude")
                    )
                ))
    }

    companion object {
        @Container
        private val db = PostGISPostgreSQLContainer()
        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) = registerDatasourceProperties(db, registry)
    }

}
