package io.spring.messenger

import org.springframework.test.context.DynamicPropertyRegistry
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.utility.DockerImageName

internal class PostGISPostgreSQLContainer : PostgreSQLContainer<PostGISPostgreSQLContainer>(
    // Consider keeping Postgis image version here in sync with docker-compose.yml's postgres service version
    DockerImageName.parse("postgis/postgis:14-3.1-alpine")
        .asCompatibleSubstituteFor("postgres")
)

internal fun registerDatasourceProperties(container: PostGISPostgreSQLContainer, registry: DynamicPropertyRegistry) {
    with(registry) {
        add("spring.datasource.url", container::getJdbcUrl)
        add("spring.datasource.username", container::getUsername)
        add("spring.datasource.password", container::getPassword)
    }
}
