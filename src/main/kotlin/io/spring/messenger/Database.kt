package io.spring.messenger

import net.postgis.jdbc.PGbox2d
import net.postgis.jdbc.PGgeometry
import net.postgis.jdbc.geometry.Point
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.Expression
import org.jetbrains.exposed.sql.ExpressionWithColumnType
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.QueryBuilder
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.append

object Messages : Table() {
    val id       = integer("id").autoIncrement()
    val content  = text("content")
    val author   = reference("author", Users.userName)
    val location = point("location").nullable()
    override val primaryKey = PrimaryKey(id)
}

object Users : Table() {
    val userName  = text("user_name")
    val firstName = text("first_name")
    val lastName  = text("last_name")
    val location  = point("location").nullable()
    override val primaryKey = PrimaryKey(userName)
}


fun Table.point(name: String, srid: Int = 4326): Column<Point>
        = registerColumn(name, PointColumnType(srid))

infix fun ExpressionWithColumnType<*>.within(box: PGbox2d) : Op<Boolean>
        = WithinOp(this, box)

private class PointColumnType(val srid: Int = 4326): ColumnType<Point>() {
    override fun sqlType() = "GEOMETRY(Point, $srid)"
    override fun valueFromDB(value: Any): Point = when {
        value is PGgeometry -> {
            val geometry = value.geometry
            when {
                geometry is Point -> geometry
                else -> error("Unexpected value of type Point: $geometry of ${geometry::class.qualifiedName}")
            }
        }
        else -> error("Unexpected value of type PGgeometry: $value of ${value::class.qualifiedName}")
    }
    override fun notNullValueToDB(value: Point): Any {
        if (value.srid == Point.UNKNOWN_SRID) value.srid = srid
        return PGgeometry(value)
    }
}

private class WithinOp(val expr1: Expression<*>, val box: PGbox2d) : Op<Boolean>() {
    override fun toQueryBuilder(queryBuilder: QueryBuilder) = queryBuilder {
        append(expr1, "&&", "ST_MakeEnvelope(${box.llb.x}, ${box.llb.y}, ${box.urt.x}, ${box.urt.y}, 4326)")
    }
}
