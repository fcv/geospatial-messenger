package io.spring.messenger.domain

import net.postgis.jdbc.geometry.Point

data class Message(
    val content  : String,
    val author   : String,
    val location : Point? = null,
    val id       : Int?   = null
)

