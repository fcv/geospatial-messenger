package io.spring.messenger.domain

import net.postgis.jdbc.geometry.Point

class User(
    var userName  : String,
    var firstName : String,
    var lastName  : String,
    var location  : Point? = null
)
